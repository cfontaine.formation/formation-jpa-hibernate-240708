package fr.dawan.requetejpa;

import java.util.List;

import fr.dawan.requetejpa.entities.Article;
import fr.dawan.requetejpa.entities.Marque;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main03SQL {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("requetejpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            List<Article> lstArticle = em.createNativeQuery("SELECT * FROM articles AS a WHERE a.prix>:prix LIMIT 2", Article.class)
                    .setParameter("prix", 50.0)
                    .getResultList();
            lstArticle.forEach(System.out::println);
            
            List<Marque> lstMarque=em.createNamedQuery("Marque.findByNom", Marque.class)
                    .setParameter("nom", "Marque A")
                    .getResultList();
            lstMarque.forEach(System.out::println);
            tx.commit();
        } catch (Exception e) {
            tx.rollback(); // transaction -> annuler
            e.printStackTrace();
        }
        em.close(); // fermeture de l'entity manager
        emf.close(); // fermeture de l'entity manager factory
    }
}
