package fr.dawan.requetejpa;

import fr.dawan.requetejpa.entities.Article;
import fr.dawan.requetejpa.entities.Marque;
import jakarta.persistence.EntityGraph;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import jakarta.persistence.Subgraph;
import jakarta.persistence.TypedQuery;

public class Main02EntityGraph {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("requetejpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        Marque mB=null;
        try {
            tx.begin();
            Marque mA=em.find(Marque.class, 1L);
            System.out.println("-----------------");
            var it=mA.getArticles().iterator();
            while(it.hasNext())
                System.out.println(it.next());
            System.out.println("-----------------");
            em.clear();
            mB=em.createQuery("FROM Marque m JOIN FETCH m.articles WHERE m.id=2",Marque.class)
                    .getSingleResult();
            System.out.println("-----------------");

//            EntityGraph<?> graph=em.createEntityGraph("marquegraph");
//            TypedQuery<Marque> queryM=em.createQuery("SELECT m FROM Marque m WHERE m.id=:id",Marque.class);
//            queryM.setParameter("id", 2L);
//            queryM.setHint("jakarta.persistence.fetchgraph",graph);
//            mB=queryM.getSingleResult();
            
            
            EntityGraph<Marque> graphDyn=em.createEntityGraph(Marque.class);
            graphDyn.addAttributeNodes("articles");
            Subgraph<Article> sub= graphDyn.addSubgraph("articles");
            sub.addAttributeNodes("fournisseurs");
            sub.addSubgraph("fournisseurs").addAttributeNodes("logo");
            TypedQuery<Marque> queryM=em.createQuery("SELECT m FROM Marque m WHERE m.id=:id",Marque.class);
            queryM.setParameter("id", 2L);
            queryM.setHint("jakarta.persistence.fetchgraph",graphDyn);
            mB=queryM.getSingleResult();
            
            tx.commit();
        } catch (Exception e) {
            tx.rollback(); 
            e.printStackTrace();
        }
        em.close(); 
//        var it=mB.getArticles().iterator();
//        while(it.hasNext())
//            System.out.println(it.next());
        System.out.println(mB.getNom());
        System.out.println(mB.getDateCreation());
        var it=mB.getArticles().iterator();
        Article af=it.next();
        System.out.println(af);
        System.out.println(af.getFournisseurs());
        emf.close(); 
    }
}
