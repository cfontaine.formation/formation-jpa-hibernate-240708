package fr.dawan.requetejpa;

import java.util.List;

import fr.dawan.requetejpa.entities.Article;
import fr.dawan.requetejpa.entities.ArticleResultat;
import fr.dawan.requetejpa.entities.Marque;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.ParameterMode;
import jakarta.persistence.Persistence;
import jakarta.persistence.Query;
import jakarta.persistence.StoredProcedureQuery;
import jakarta.persistence.TypedQuery;

public class Main01JPQL {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("requetejpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();

            // En SQL: SELECT * FROM articles
            // En JPQL: SELECT a FROM Article as a

            // createQuery -> permet de créer les requêtes JPQL depuis l’Entity Manager
            // getSingleResult -> pour récupérer un résultat unique, s'il n'y a pas de
            // résulat ou plusieurs => exception
            // getResultList() -> pour récupérer un résultat multiple (List)

            // Query et TypedQuery: représente la requête
            // Query
            Query query1 = em.createQuery("SELECT a FROM Article as a");
            List<Article> ar = query1.getResultList();
            ar.forEach(System.out::println);

            // TypedQuery -> à privilégier par rapport à Query => Type Générique
            // TypedQuery<Article> query2= em.createQuery("SELECT a FROM Article as a",
            // Article.class);
            TypedQuery<Article> query = em.createQuery("FROM Article as a", Article.class);
            query.getResultList().forEach(System.out::println);

            TypedQuery<String> query3 = em.createQuery("SELECT a.description FROM Article as a", String.class);
            query3.getResultList().forEach(System.out::println);

            TypedQuery<Object[]> query4 = em.createQuery("SELECT a.description, a.prix FROM Article as a",
                    Object[].class);
            query4.getResultList().forEach(tabObj -> System.out.println(tabObj[0] + " " + tabObj[1]));

            // new
            TypedQuery<ArticleResultat> query5 = em.createQuery(
                    "SELECT new ArticleResultat(a.description,a.prix) FROM Article as a", ArticleResultat.class);
            query5.getResultList().forEach(System.out::println);

            // DISTINCT
            TypedQuery<String> query6 = em.createQuery("SELECT DISTINCT a.description FROM Article as a", String.class);
            query6.getResultList().forEach(System.out::println);

            // WHERE
            query = em.createQuery("SELECT a FROM Article a WHERE a.prix<50.0", Article.class);
            query.getResultList().forEach(System.out::println);

            // Paramètres de requête

            // position -> ?numero_position
            query = em.createQuery("SELECT a FROM Article a WHERE a.prix<?1 AND a.prix>?2", Article.class);
            query.setParameter(1, 100.0); // -> setParameter, pour définir la valeur des paramètres
            query.setParameter(2, 30.0);
            query.getResultList().forEach(System.out::println);

            // paramètre nommés -> :nomParamètre
            query = em.createQuery("SELECT a FROM Article a WHERE a.prix<:prixMax AND a.prix>:prixMin", Article.class);
            query.setParameter("prixMax", 100.0);
            query.setParameter("prixMax", 100.0);
            query.setParameter("prixMin", 30.0);
            query.getResultList().forEach(System.out::println);

            // Opérateurs
            // BETWEEN -> définir un interval
            query = em.createQuery("SELECT a FROM Article a WHERE a.prix BETWEEN :prixMin AND :prixMax", Article.class);
            query.setParameter("prixMax", 100.0);
            query.setParameter("prixMin", 30.0);
            query.getResultList().forEach(System.out::println);

            // IN -> définir un ensemble de valeur
            query = em.createQuery("SELECT a FROM Article a WHERE a.prix IN (40.0,80.0,120)", Article.class);
            query.getResultList().forEach(System.out::println);

            // LIKE -> recherche sur un modèle particulier
            // Jokers: % -> 0,1 ou n caractères, – _ -> 1 caractère
            // Ici, on recherche tous les articles dont la description commence par M et
            // fait au moins 4 caractères
            query = em.createQuery("SELECT a FROM Article a WHERE a.description LIKE :modele", Article.class);
            query.setParameter("modele", "M____%");
            query.getResultList().forEach(System.out::println);

            // ESCAPE -> pour utiliser % ou _ en temps que caractère pour qu'ils ne soient
            // pas interprété comme un Joker
            query = em.createQuery("SELECT a FROM Article a WHERE a.description LIKE '%100@%%' ESCAPE '@'",
                    Article.class);
            query.getResultList().forEach(System.out::println);

            // IS NULL -> pour tester si un variable d'instance est null
            query = em.createQuery("SELECT a FROM Article a WHERE a.marque IS NULL", Article.class);
            query.getResultList().forEach(System.out::println);

            // IS EMPTY -> permet de tester, si une colection est vide
            TypedQuery<Marque> queryM = em.createQuery("SELECT m FROM Marque m WHERE m.articles IS EMPTY",
                    Marque.class);
            queryM.getResultList().forEach(System.out::println);

            // Expression de chemin
            // Pour un @OneToOne, @ManyToOne
            // marque est une "simple" variable d'instense, on peut accéder au nom
            query = em.createQuery("SELECT a FROM Article a WHERE a.marque.dateCreation < {d '1960-01-01'}",
                    Article.class);
            query.getResultList().forEach(System.out::println);

            // @ManyToMany et @OneToMany -> ERREUR
            // ERREUR articles est une collection => il faut faire une jointure
//            TypedQuery<Marque> querypath2= em.createQuery("SELECT m FROM Marque m WHERE m.articles.prix < 50.0", Marque.class);
//            querypath2.getResultList().forEach(System.out::println);

            // Fonctions d'agrégations
            // COUNT
            TypedQuery<Long> queryLong = em.createQuery("SELECT COUNT(a) FROM Article a WHERE a.prix>50.0 ",
                    Long.class);
            System.out.println(queryLong.getSingleResult());

            // AVG
            TypedQuery<Double> queryDouble = em.createQuery("SELECT AVG(a.prix) FROM Article a", Double.class);
            System.out.println(queryDouble.getSingleResult());

            // SIZE
            TypedQuery<Integer> queryInt = em.createQuery("SELECT SIZE(m.articles) FROM Marque m WHERE m.id=1",
                    Integer.class);
            System.out.println(queryInt.getSingleResult());

            // Fonction chaine de caractères
            TypedQuery<String> queryStr = em.createQuery(
                    "SELECT CONCAT(a.description, ' : ', LENGTH(a.description)) FROM Article a", String.class);
            queryStr.getResultList().forEach(System.out::println);

            queryStr = em.createQuery("SELECT UPPER(SUBSTRING(a.description,1,4)) FROM Article a", String.class);
            queryStr.getResultList().forEach(System.out::println);

            queryInt = em.createQuery("SELECT LOCATE('Tv',a.description) FROM Article a", Integer.class);
            queryInt.getResultList().forEach(System.out::println);

            // Fonction temporelle
            queryInt = em.createQuery("SELECT EXTRACT(QUARTER FROM  m.dateCreation ) FROM Marque m", Integer.class);
            queryInt.getResultList().forEach(System.out::println);

            queryInt = em.createQuery(
                    "SELECT EXTRACT(YEAR FROM  LOCAL_DATE() )-EXTRACT(YEAR FROM  m.dateCreation ) FROM Marque m",
                    Integer.class);
            queryInt.getResultList().forEach(System.out::println);

            // Trie -> ORDER BY
            query = em.createQuery("SELECT a FROM Article a ORDER BY a.description,a.prix DESC", Article.class);
            query.getResultList().forEach(System.out::println);

            // GROUP BY
            queryStr = em.createQuery("SELECT  CONCAT(a.marque.nom, ' : ',COUNT(a.id))FROM Article a GROUP BY a.marque",
                    String.class);
            queryStr.getResultList().forEach(System.out::println);

            // HAVING
            queryStr = em.createQuery(
                    "SELECT  CONCAT(a.marque.nom, ' : ',COUNT(a.id))FROM Article a GROUP BY a.marque HAVING COUNT(a.id) >3",
                    String.class);
            queryStr.getResultList().forEach(System.out::println);

            // Jointure interne JOIN
            queryStr = em.createQuery(
                    "SELECT CONCAT(a.description,' ', a.prix,' ',m.nom) FROM Marque m JOIN m.articles a", String.class);
            queryStr.getResultList().forEach(System.out::println);

            queryStr = em.createQuery(
                    "SELECT CONCAT(a.description,' ', a.prix,' ',a.marque.nom) FROM Article a JOIN a.fournisseurs f WHERE f.nom='Fournisseur 1'",
                    String.class);
            queryStr.getResultList().forEach(System.out::println);

            // Jointure externe LEFT JOIN
            queryStr = em.createQuery(
                    "SELECT CONCAT(a.description,' ', a.prix) FROM Article a LEFT JOIN a.marque m WHERE m IS NULL",
                    String.class);
            queryStr.getResultList().forEach(System.out::println);

            // Sous-requete qui retourne un seule resultat
            query = em.createQuery("SELECT a FROM Article a WHERE a.prix <(SELECT AVG(a2.prix) FROM Article a2)",
                    Article.class);
            query.getResultList().forEach(System.out::println);

            // Exist
            query = em.createQuery(
                    "SELECT a FROM Article a WHERE NOT EXISTS (SELECT m FROM Marque m JOIN m.articles ar WHERE ar = a)",
                    Article.class);
            query.getResultList().forEach(System.out::println);

            // ANY
            query = em.createQuery(
                    "SELECT a FROM Article a WHERE a.prix <= ANY (SELECT a1.prix FROM Article a1 WHERE a1.marque.nom= :marqueNom)",
                    Article.class);
            query.setParameter("marqueNom", "Marque A");
            query.getResultList().forEach(System.out::println);

            // SOME
            query = em.createQuery(
                    "SELECT a FROM Article a WHERE a.prix <= SOME (SELECT a1.prix FROM Article a1 WHERE a1.marque.nom= :marqueNom)",
                    Article.class);
            query.setParameter("marqueNom", "Marque A");
            query.getResultList().forEach(System.out::println);

            // ALL
            query = em.createQuery(
                    "SELECT a FROM Article a WHERE a.prix <= ALL (SELECT a1.prix FROM Article a1 WHERE a1.marque.nom= :marqueNom)",
                    Article.class);
            query.setParameter("marqueNom", "Marque A");
            query.getResultList().forEach(System.out::println);

            // Requête nommées
            TypedQuery<Article> queryName = em.createNamedQuery("Article.prixless", Article.class);
            queryName.setParameter(1, 25.0);
            queryName.getResultList().forEach(System.out::println);

            queryName = em.createNamedQuery("Article.descriptionPrefix", Article.class);
            queryName.setParameter("modele", "C");
            queryName.getResultList().forEach(System.out::println);

            // setMaxResult -> LIMIT en SQL
            query = em.createQuery("SELECT a FROM Article a ORDER BY a.prix DESC", Article.class);
            query.setMaxResults(5);
            query.getResultList().forEach(System.out::println);

            // pagination
            long nbArticle = em.createQuery("SELECT count(a) FROM Article a", Long.class).getSingleResult();
            for (int i = 0; i < nbArticle; i += 5) {
                em.createQuery("SELECT a FROM Article a ORDER BY a.prix DESC", Article.class).setFirstResult(i)
                        .setMaxResults(i + 5).getResultList().forEach(System.out::println);
            }

            // UPDATE
            Query queryUpdate = em.createQuery( "UPDATE Article a SET a.prix=a.prix*:pourcentAugmentation WHERE a.prix BETWEEN :prixMin AND :prixMax  ");
            queryUpdate.setParameter("pourcentAugmentation", 1.1);
            queryUpdate.setParameter("prixMin", 35.0);
            queryUpdate.setParameter("prixMax", 200.00);
            int nbMajPrix = queryUpdate.executeUpdate();
            System.out.println(nbMajPrix);

            // DELETE
            Query queryDelete = em.createQuery( "DELETE FROM Article a WHERE a.marque IS NULL");
            int nbSupArticle = queryDelete.executeUpdate();
            System.out.println(nbSupArticle);
            
            // Procedure stockée
            StoredProcedureQuery procedureQuery=em.createStoredProcedureQuery("addition");
            procedureQuery.registerStoredProcedureParameter("a", Integer.class, ParameterMode.IN);
            procedureQuery.registerStoredProcedureParameter("b", Integer.class, ParameterMode.IN);
            procedureQuery.registerStoredProcedureParameter("somme", Integer.class, ParameterMode.OUT);
            
            procedureQuery.setParameter("a", 2);
            procedureQuery.setParameter("b", 3);
            procedureQuery.execute();
            
            int somme=(Integer)procedureQuery.getOutputParameterValue("somme");
            System.out.println(somme);
            
            procedureQuery=em.createNamedStoredProcedureQuery("Article.nbArticleByMarque");
            procedureQuery.setParameter("id_marque", 3L);
            procedureQuery.execute();
            int nb_article=(Integer) procedureQuery.getOutputParameterValue("nb_article");
            System.out.println(nb_article);
            
            tx.commit();
        } catch (Exception e) {
            tx.rollback(); // transaction -> annuler
            e.printStackTrace();
        }
        em.close(); // fermeture de l'entity manager
        emf.close(); // fermeture de l'entity manager factory
    }
}
