package fr.dawan.requetejpa.entities;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
public class Logo extends BaseEntity {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String nom;
    
    @OneToOne(mappedBy = "logo")
    @Exclude
    private Fournisseur fournisseur;
}
