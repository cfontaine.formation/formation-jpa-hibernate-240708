package fr.dawan.requetejpa.entities;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.envers.RelationTargetNotFoundAction;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.NamedAttributeNode;
import jakarta.persistence.NamedEntityGraph;
import jakarta.persistence.NamedEntityGraphs;
import jakarta.persistence.NamedNativeQueries;
import jakarta.persistence.NamedNativeQuery;
import jakarta.persistence.NamedSubgraph;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@ToString

//@NamedEntityGraphs(value= {
//        @NamedEntityGraph(name="marquegraph",
//                attributeNodes= {
//                        @NamedAttributeNode(value = "articles",subgraph = "sub_article_fournisseur")
//                }
//        , subgraphs = {
//                    @NamedSubgraph(name="sub_article_fournisseur",
//                                    attributeNodes = @NamedAttributeNode(value="fournisseurs",subgraph="sub_logo_fournisseur")),
//                    @NamedSubgraph(name="sub_logo_fournisseur",
//                                            attributeNodes =  @NamedAttributeNode("logo") )                    
//                    })
//        })
     

@NamedNativeQueries(value= {
        @NamedNativeQuery(name="Marque.findByNom",
                          query="SELECT * FROM marques m WHERE m.nom = :nom",
                          resultClass = Marque.class)
        
})


@Entity
@Table(name = "marques")
@Audited(withModifiedFlag = true )
public class Marque extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @NonNull
    @Column(length = 60, nullable = false)
    private String nom;

    @NonNull
    @Column(name="date_creation")
    private LocalDate dateCreation;
    
    @Exclude
    @OneToMany(mappedBy = "marque", cascade= {CascadeType.PERSIST,CascadeType.REMOVE}, orphanRemoval = true)
    private Set<Article> articles=new HashSet<>();
    
    // @OneToMany unidirectionel, il faut ajouter un @JoinColumn
//    @OneToMany
//    @JoinColumn(name="marque_id")
//    private Set<Article> articles=new HashSet<>();
}
