package fr.dawan.requetejpa.entities;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.envers.RelationTargetNotFoundAction;

import fr.dawan.requetejpa.enums.Emballage;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.NamedStoredProcedureQueries;
import jakarta.persistence.NamedStoredProcedureQuery;
import jakarta.persistence.ParameterMode;
import jakarta.persistence.StoredProcedureParameter;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)


@NamedQueries(value={
    @NamedQuery(name="Article.prixless",
                query="SELECT a FROM Article a WHERE a.prix <?1"),
    @NamedQuery(name="Article.descriptionPrefix",
                query= "SELECT a FROM Article a WHERE a.description LIKE CONCAT(:modele,'%' )")
    }
)

@NamedStoredProcedureQueries(value= {
        @NamedStoredProcedureQuery(name="Article.nbArticleByMarque",procedureName = "count_article_by_marque",
        parameters = {
                @StoredProcedureParameter(name="id_marque",mode=ParameterMode.IN,type=Long.class),
                @StoredProcedureParameter(name="nb_article",mode=ParameterMode.OUT,type=Integer.class),
            }
        )
    }
)



@Entity
@Table(name="articles")
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
public class Article extends BaseEntity {

    private static final long serialVersionUID = 1L;
    
    @Column(length = 100,nullable=false)
    private String description;
    
    private double prix;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private Emballage emballage;
    
    @ManyToOne(/*fetch=FetchType.LAZY*/)
    @JoinColumn(name="fk_marque")
    private Marque marque;
    
    @ManyToMany(mappedBy = "articles")
    @Exclude
    private Set<Fournisseur> fournisseurs = new HashSet<>();

    public Article(String description, double prix, Emballage emballage) {
        super();
        this.description = description;
        this.prix = prix;
        this.emballage = emballage;
    }

}
