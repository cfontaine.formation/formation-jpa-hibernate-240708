package fr.dawan.requetejpa;

import java.util.List;

import fr.dawan.requetejpa.entities.Article;
import fr.dawan.requetejpa.entities.Fournisseur;
import fr.dawan.requetejpa.entities.Marque;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import jakarta.persistence.Tuple;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaDelete;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.CriteriaUpdate;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.Subquery;

public class Main04Criteria {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("requetejpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            // EN JPQL : SELECT a FROM Article a
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Article> cq = cb.createQuery(Article.class);
            Root<Article> ar = cq.from(Article.class);
            cq.select(cq.getSelection());
            TypedQuery<Article> tq = em.createQuery(cq);
            tq.getResultList().forEach(System.out::println);

            // EN JPQL : SELECT a FROM Article a WHERE a.prix<
            CriteriaQuery<Article> cq2 = cb.createQuery(Article.class);
            Root<Article> ar2 = cq2.from(Article.class);
            cq2.select(cq2.getSelection()).where(cb.lessThan(ar2.get("prix"), 150.0)); // ar2.get(Article_.prix)
            tq = em.createQuery(cq2);
            tq.getResultList().forEach(System.out::println);

            // EN JPQL : SELECT a.description FROM Article as a

            CriteriaQuery<String> cq3 = cb.createQuery(String.class);
            Root<Article> ar3 = cq3.from(Article.class);
            cq3.select(ar3.get("description"));
            TypedQuery<String> tStr = em.createQuery(cq3);
            tStr.getResultList().forEach(System.out::println);

            // Distinct
            cq3 = cb.createQuery(String.class);
            ar3 = cq3.from(Article.class);
            cq3.select(ar3.get("description"));
            cq3.distinct(true);
            tStr = em.createQuery(cq3);
            tStr.getResultList().forEach(System.out::println);

            // Predicate
            cq = cb.createQuery(Article.class);
            ar = cq.from(Article.class);
            Predicate predicateinfprix = cb.lessThan(ar.get("prix"), 200.0);
            Predicate predicatesupprix = cb.greaterThan(ar.get("prix"), 50.0);
            Predicate predicatPrix = cb.and(predicateinfprix, predicatesupprix);
            cq.select(ar).where(predicatPrix);
            tq = em.createQuery(cq);
            tq.getResultList().forEach(System.out::println);
            
            
            // BETWEEN 
            cq = cb.createQuery(Article.class);
            ar = cq.from(Article.class);
            cq.select(ar).where(cb.between(ar.get("prix"), 50.0, 200.0));
            tq = em.createQuery(cq);
            tq.getResultList().forEach(System.out::println);
            
            // IN
            cq = cb.createQuery(Article.class);
            ar = cq.from(Article.class);
            cq.select(ar).where(ar.get("prix").in(350.0,19.9,650.0));
            tq = em.createQuery(cq);
            tq.getResultList().forEach(System.out::println);
            
            // LIKE
            cq = cb.createQuery(Article.class);
            ar = cq.from(Article.class);
            cq.select(ar).where(cb.like(ar.get("description"), "M_____%"));
            tq = em.createQuery(cq);
            tq.getResultList().forEach(System.out::println);
            
            // Escape
            cq = cb.createQuery(Article.class);
            ar = cq.from(Article.class);
            cq.select(ar).where(cb.like(ar.get("description"), "%100@%%",'@'));
            tq = em.createQuery(cq);
            tq.getResultList().forEach(System.out::println);
            
            //IS NULL
            cq = cb.createQuery(Article.class);
            ar = cq.from(Article.class);
            cq.select(ar).where(cb.isNull(ar.get("marque")));
            tq = em.createQuery(cq);
            tq.getResultList().forEach(System.out::println);
            
            // Order BY
            cq = cb.createQuery(Article.class);
            ar = cq.from(Article.class);
            cq.select(ar)
              .where(cb.between(ar.get("prix"), 50.0, 200.0))
              .orderBy(cb.asc(ar.get("description")), cb.desc(ar.get("prix")));
            tq = em.createQuery(cq);
            tq.getResultList().forEach(System.out::println);
            
            // COUNT
            CriteriaQuery<Long> cql= cb.createQuery(Long.class);
            ar = cql.from(Article.class);
            cql.select(cb.count(ar));
            TypedQuery<Long> tql = em.createQuery(cql);
            System.out.println(tql.getSingleResult());
            
            // GroupBY
            cql= cb.createQuery(Long.class);
            ar = cql.from(Article.class);
            cql.select(cb.count(ar))
            .groupBy(ar.get("marque"));

            tql = em.createQuery(cql);
            tql.getResultList().forEach(System.out::println);
            
            
            // Having
            CriteriaQuery<Object[]> cqObj=cb.createQuery(Object[].class);
            ar= cqObj.from(Article.class);
            
            cqObj.multiselect(
                    cb.avg(ar.get("prix")),
                    cb.count(ar)
            ).groupBy(ar.get("marque"))
            .having(cb.greaterThan(cb.count(ar), 3L));
            
            TypedQuery<Object[]> tqObj=em.createQuery(cqObj);
            tqObj.getResultList().forEach(o -> System.out.println(o[0] + " " +o[1]));
            
            // TUPPLE
            CriteriaQuery<Tuple> cqt=cb.createTupleQuery();
            ar = cqt.from(Article.class);
   
            cqt.multiselect(ar.get("marque").get("nom").alias("nomMarque"),
                            cb.count(ar).alias("nombreArticle")
            ).groupBy(ar.get("marque"));
            List<Tuple>tt=em.createQuery(cqt).getResultList();
            tt.forEach(t -> System.out.println(t.get("nomMarque") + " :" +t.get("nombreArticle") ));
            
            
            // Expression Path
            cq=cb.createQuery(Article.class);
            ar=cq.from(Article.class);
            cq.select(ar)
            .where(cb.equal(ar.get("marque").get("nom"),"Marque A"));
            em.createQuery(cq).getResultList().forEach(System.out::println);
            
            // JOIN
            cq=cb.createQuery(Article.class);
            ar=cq.from(Article.class);
            Join<Article, Fournisseur> fj=ar.join("fournisseurs",JoinType.INNER);
            cq.select(ar)
            .where(cb.equal(fj.get("nom"),"Fournisseur 1"));
            em.createQuery(cq).getResultList().forEach(System.out::println);
                    
            // LEFT JOIN
            cq=cb.createQuery(Article.class);
            ar=cq.from(Article.class);
            Join<Article, Marque> fm=ar.join("marque",JoinType.LEFT);
            cq.select(ar)
            .where(cb.isNull(fm));
            em.createQuery(cq).getResultList().forEach(System.out::println);
            
            // Sous-requête
            //SELECT a FROM Article a WHERE a.prix <(SELECT AVG(a2.prix) FROM Article a2)
            cq=cb.createQuery(Article.class);
            ar=cq.from(Article.class);
            Subquery<Double> sub1 =cq.subquery(Double.class);
            Root<Article> subroot1=sub1.from(Article.class);
            sub1.select(cb.avg(subroot1.get("prix")));
            cq.select(ar)
            .where(cb.greaterThan(ar.get("prix"), sub1));
            em.createQuery(cq).getResultList().forEach(System.out::println);
            
            // "SELECT a FROM Article a WHERE a.prix <= ALL (SELECT a1.prix FROM Article a1 WHERE a1.marque.nom= :marqueNom)"
            cq=cb.createQuery(Article.class);
            ar=cq.from(Article.class);
            Subquery<Double> sub2 =cq.subquery(Double.class);
            Root<Article> subroot2=sub2.from(Article.class);
            sub2.select(subroot2.get("prix"))
            .where(cb.equal( subroot2.get("marque").get("nom"),"Marque B"));
            
            // UPDATE
            CriteriaUpdate<Article> cu=cb.createCriteriaUpdate(Article.class);
            Root<Article> rootA=cu.from(Article.class);
            cu.set("prix", 15.0);
            cu.where(cb.lessThan(rootA.get("prix"), 20.0));
            int nb_article=em.createQuery(cu).executeUpdate();
            System.out.println(nb_article);
            
            // DELETE
            CriteriaDelete<Article> cd=cb.createCriteriaDelete(Article.class);
            Root<Article> rootD=cd.from(Article.class);
            cd.where(cb.isNull(rootD.get("marque")));
            nb_article=em.createQuery(cd).executeUpdate();
            System.out.println(nb_article);
            
            tx.commit();
        } catch (Exception e) {
            tx.rollback(); // transaction -> annuler
            e.printStackTrace();
        }
        em.close(); // fermeture de l'entity manager
        emf.close(); // fermeture de l'entity manager factory
    }
}
